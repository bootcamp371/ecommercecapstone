function quantityButtonDown(btn) {
    btn.parentNode.querySelector('input[type=number]').stepDown();
}

function quantityButtonUp(btn) {
    btn.parentNode.querySelector('input[type=number]').stepUp();
}