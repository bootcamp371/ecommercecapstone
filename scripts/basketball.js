/*On click listener for submitCheckout button; shows a confirmation modal if validInputs() function returns true and stops event propogation so modal persists*/
document.getElementById('submitCheckout').onclick = function (event) {
    const confirmationModal = new bootstrap.Modal(document.getElementById("confirmationModal"), {});
    confirmationModal.show();
    event.preventDefault();
    event.stopPropagation();
}
