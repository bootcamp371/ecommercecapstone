# E-Commerce Capstone Project

The purpose of this project is to get us good working knowledge of HTML, CSS and bootstrap. We need to make a ecommerce site that shows we have an understanding of some of the basic concepts of web design. 

## Hall of Fame Baseball Cards
Hall of Fame Baseball Cards is an amazing site that will obviously be the benchmark for all future website design. Only a fool would think that block lettering and borders around everything would not be the wave of the future. 

## Description
This site was designed with certain objectives in mind. We were required to use:
1. Tables
2. Lists
3. validation
4. A catalog of at least 6 items
5. Bootstrap Cards
6. A stand alone image
7. Bootstrap Grid
8. Navbar

Throughout the project we need to utililize github to show an understanding of version control while using the tool.

## Usage
On the product pages, you can make the screen bigger and smaller and watch the cards get bigger and smaller along with realigning itself when it gets to break points of col-md-4 col-sm-6 col-lg-3. Also you can see the validation that is in place when using the login, register and checkout pages.

## Screenshots
![Image](/images/screenshots/homepage.PNG "Home Page")
![Image](/images/screenshots/baseballpage.PNG "Baseball Page")
![Image](/images/screenshots/baseketballpage.PNG "Basketball Page")
![Image](/images/screenshots/footballpage.PNG "Football Page")
![Image](/images/screenshots/loginpage.PNG "Login Page")
![Image](/images/screenshots/registerpage.PNG "Register Page")
![Image](/images/screenshots/checkoutpage.PNG "Checkout Page")
![Image](/images/screenshots/thankyoupage.PNG "Thank You Page")

## Support
If you need help readjusting the size of the scren, please call the support desk and tell them you have an ID-10T error.

## Roadmap
I am not sure that there will be further updates, but Javascript and Angular are coming, so it would be fun to bring this site to life, however I don't see 12 of us all using different sites for this.

## Contributing
If you want to contribute to this site, please email me at mark.galasso@centene.com and we can discuss the fees for being part of such an amazing project.

## Authors and acknowledgment
Auther: Mark Galasso
Special acknowledgement to Maaike and the rest of the Cohort team 1. This could not have been done without you. Also to the real Hall of Fame Baseball cards. They had some really cool logos and favicons that I stole from them.

## Project status
Well, by the time you are reading this, it will be time for presentation. Any further updates would be up to Maaike. 
